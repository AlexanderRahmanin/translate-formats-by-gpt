const { GoogleSpreadsheet } = require("google-spreadsheet");
const creds = require("./avid-catalyst-330412-c778c3065845.json"); //Данные от сервисного аккаунта https://habr.com/ru/articles/488756/#avtorizaciya-cherez-servisnyy-akkaunt

const doc = new GoogleSpreadsheet(
    "16ibo8cGeQVd-16RwIYdNyFM9adaul19j7YUXhI9xb7s" //id таблицы https://w6p.ru/MGEzZjc.png
  );

const config = {
    sheetName: "Fr", //Какой лист создавать в таблице с результатами перевода
    langToTranslete: "French" //На какой лист переводить
}
  




  const chatgpt = async (prompt) => {
    try {
        const { ChatGPTAPI } = await import('chatgpt');
      
      const api = new ChatGPTAPI({
          apiKey: "sk-XCXYMaasdf97j2sKrl9BdsgeiYT3BlbksdgsdggsdFJTgXSnJPmzfdsAFVwRH3vjHn" //API ключ для chatGPT https://timeweb.com/ru/community/articles/kak-poluchit-api-klyuch-v-chatgpt
        })
      
      const res = await api.sendMessage(prompt)

      return res.text;
    } catch (error) {
      console.log("Ошибка с gpt API")
      console.log(error);
      return "Ошибка"
    }
    

  }
  

const getData = async () => {
    await doc.useServiceAccountAuth(creds);
    await doc.loadInfo();
    const sheet = doc.sheetsByTitle["EN"];  //EN - это название листа в таблице
    return await sheet.getRows();
  };

const parseData = (data) => {
    let parsedData = [];
    data.forEach((element) => {
        
        parsedData.push({
            "Название": element['Название'],
            "Описание": element['Описание'],
            "Технические детали": element['Технические детали']
        }) //В таблице есть 3 столбца: Название, Описание, Технические детали. Создаем объект с содержимым этих столбцов.
    });

    return parsedData;

}

const createSheet = async () => {
	try{
  await doc.loadInfo();
    const sheet = await doc.addSheet({ headerValues: ["Название", "Описание", "Технические детали"] }); //Созем на новом листе заголовки Название, Описание, Технические детали
    await sheet.updateProperties({ title: config.sheetName});
    return sheet;



  }catch(e){
    console.log(e);

  }
	
	
}

const sendData = async (data, sheet) => {

  try {
    await sheet.addRows([data]);
    }catch (error) {
      console.log("Ошибка при добавлении в таблицу")
      console.log(error)
      
    }

  }; 

  const translateData = async (data, sheet) => {
  
    for (let index = 0; index < data.length; index++) {
      const element = data[index];
  
      console.log(`Переводим строку №${index + 1}`);
  
      await sleep(5);
      
      await sendData({
        "Название": element["Название"],
        "Описание": element["Описание"] ? await chatgpt(`Translate to ${config.langToTranslete}\n` + element['Описание']) : "",
        "Технические детали": element["Технические детали"] ? await chatgpt(`Translate to ${config.langToTranslete}\n` + element['Технические детали']) : "",
      }, sheet);

    }
  
  };

const main = async () => {
    const data = await getData();
    const parsedData = parseData(data);
    const sheet = await createSheet();
    await translateData(parsedData, sheet)
    console.log("Готово");


}



main()