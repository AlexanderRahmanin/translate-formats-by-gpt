#  Translate Formats
Node.js скрипт для перевода на различные языки описаний форматов из Google таблицы с помощью chatGPT.

## Подготовительные работы
- Вам необходимо создать и подключить к скрипту сервисный аккаунт Google. Вот [инструкция](https://habr.com/ru/articles/488756/#avtorizaciya-cherez-servisnyy-akkaunt). Полученный файл кладем в папку app и [прописываем](https://w6p.ru/NTQ1NDM.png) его имя в index.js;
- Вам необходимо получить API ключ chatGPT. Вот [инструкция](https://timeweb.com/ru/community/articles/kak-poluchit-api-klyuch-v-chatgpt). [Прописываем ключ](https://w6p.ru/M2QzMGI.png) в index.js;
- Указываем в скрипте [id таблицы](https://w6p.ru/ZjA5MzM.png);
- [Настраиваем язык](https://w6p.ru/NjY3M2Y.png), на который будем переводить;


## Запуск скрипта с Docker Compose
Нужен [Docker Compose](https://www.youtube.com/watch?v=a5mxBTGfC5k)
```
cd “папка скрипта”
docker-compose up
```

## Запуск скрипта без Docker Compose
Нужена версия Node.js 18 и выше
Устанавливаем зависимости. Делается 1 раз
```
cd “папка скрипта”/app
npm install
```

Запускаем скрипт
```
cd “папка скрипта”/app
node index.js
```
